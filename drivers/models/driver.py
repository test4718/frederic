from django.db import models


class Driver(models.Model):
    first_name = models.TextField(max_length=128)
    last_name = models.TextField(max_length=128)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'drivers'
