from json import loads
from requests import get
from frederic.env import env

from frederic.find.locations import Locations

from stations.models.station import Station as StationM
from stations.serializers.station import ListSerializer as StationListSerializer
from drivers.serializers.driver import ListSerializer as DriverListSerializer


class Route():

    def __init__(self, x: int, y: int, radius: list):
        self.radius_list = radius
        self.best = {"distance": 10000}
        self.list = []
        self._eval(x, y)

    def _eval(self, x, y):
        _filters = {
            "coordinates": f"{x},{y}"
        }
        drivers = DriverListSerializer(
            loads(get(env("LOCATIONS")).text), many=True, context={"filters": _filters}).data
        stations = StationListSerializer(
            StationM.objects.all(), many=True, context={"filters": _filters}).data

        stations = self._find(stations, f"{x},{y}")
        for station in stations:
            drivers = self._find(drivers, f"{station['x']},{station['x']}")
            for driver in drivers:
                _station = driver.copy()
                _station["distance"] += driver["distance"]
                _station["driver"] = driver
                _station["arrival_time"] = _station["distance"] / \
                    env("SPEED_DRIVER", int)
                self.list.append(_station)

                if _station["distance"] < self.best["distance"]:
                    self.best = _station

    def _find(self, locations: list, coordinates: str):
        for radius in self.radius_list:
            result = Locations(data=locations, filters={
                "radius": radius,
                "coordinates": coordinates
            }).data
            if result:
                return result


# from frederic.find.drivers import Driver
# from frederic.find.stations import Station
# from frederic.env import env


# class Route():

#     def __init__(self, x: int, y: int, radius: list):
#         self.list = []
#         self.best = {"distance": 10000}
#         self._eval(x, y, radius)

#     def _eval(self, x, y, radius_list):
#         stations = Station().search(x, y, radius_list)


#         for station in stations:
#             drivers = Driver().search(station["x"], station["y"], radius_list)
#             for driver in drivers:
#                 _station = driver.copy()
#                 _station["distance"] += driver["distance"]
#                 _station["driver"] = driver
#                 _station["arrival_time"] = _station["distance"]/env("SPEED_DRIVER", int)
#                 self.list.append(_station)

#                 if _station["distance"] < self.best["distance"]:
#                     self.best = _station


#     def _find(self, )
