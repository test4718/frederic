from django.db import models
from orders.models.order import Order
from orders.models.state_type import StateType

class State(models.Model):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    state_type = models.ForeignKey(StateType, on_delete=models.PROTECT)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'orders'