from django.db import transaction
from rest_framework import serializers

from stations.models.station import Station


class ListSerializer(serializers.ModelSerializer):
    distance = serializers.SerializerMethodField()

    def get_distance(self, data):
        if "coordinates" in self.context["filters"]:
            x, y = self.context["filters"]["coordinates"].split(",")
            y = abs(int(y) - int(data.y))
            x = abs(int(x) - int(data.x))
            return x+y
        return 0

    class Meta:
        model = Station
        fields = [
            "name",
            "x",
            "y",
            "distance"
        ]
        depth = 1
