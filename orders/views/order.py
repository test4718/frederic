from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from frederic.env import env

from orders.filters.order import OrderFilter
from orders.serializers.order import ListSerializer, CreateSerializer
from orders.models.order import Order


class OrderView(GenericAPIView):

    def get(self, request):
        orders = OrderFilter(request.query_params,
                             queryset=Order.objects.all())
        return Response(ListSerializer(orders.qs, many=True).data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = serializer.save(raise_exception=True)
        return Response(ListSerializer(order, many=False).data, status=status.HTTP_201_CREATED)
