from json import loads
from requests import get

from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status

from frederic.find.locations import Locations
from frederic.env import env
from drivers.serializers.driver import ListSerializer



class DriverView(GenericAPIView):

    def get(self, request):
        drivers = ListSerializer(
            loads(get(env("LOCATIONS")).text), many=True, context={"filters": request.query_params}).data

        return Response(Locations(drivers, request.query_params).data, status=status.HTTP_200_OK)
