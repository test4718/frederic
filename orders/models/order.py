from django.db import models
from drivers.models.driver import Driver


class Order(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)
    date_time = models.DateTimeField()
    x = models.IntegerField()
    y = models.IntegerField()
    arrival_time_min = models.IntegerField()

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'orders'
