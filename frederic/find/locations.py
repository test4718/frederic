from json import loads
from frederic.env import env
from drivers.serializers.driver import ListSerializer


class Locations():

    def __init__(self, data: list = [], filters: dict = {}):
        self._filters = filters
        self.data = list(filter(self._filter, data))
        self.data.sort(key=lambda item: item["distance"])

    def _filter(self, item) -> bool:
        _filters = self._filters
        results = []
        if "coordinates" in _filters and "radius" in _filters:
            results.append(self._filter_radius(
                item, _filters["coordinates"], _filters["radius"]))

        return (not False in results)

    def _filter_radius(self, item: dict = {}, coordinates: str = "", radius: int = 3):
        x, y = coordinates.split(",")
        return (
            (
                item["x"] >= int(x)-int(radius) and
                item["y"] >= int(y)-int(radius)
            ) and
            (
                item["x"] <= int(x)+int(radius) and
                item["y"] <= int(y)+int(radius)
            )
        )
