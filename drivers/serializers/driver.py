from rest_framework import serializers

from drivers.models.driver import Driver


class ListModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Driver
        fields = (
            "first_name",
            "last_name"
        )
        depth = 1


class ListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    x = serializers.IntegerField()
    y = serializers.IntegerField()
    user = serializers.SerializerMethodField()
    distance = serializers.SerializerMethodField()

    def get_user(self, data):
        return ListModelSerializer(Driver.objects.get(id=data["id"]), many=False).data

    def get_distance(self, data):
        if "coordinates" in self.context["filters"]:
            x, y = self.context["filters"]["coordinates"].split(",")
            y = abs(int(y) - int(data["y"]))
            x = abs(int(x) - int(data["x"]))
            return x+y
        return 0
