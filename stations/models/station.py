from django.db import models


class Station(models.Model):
    name = models.TextField(max_length=128)
    x = models.IntegerField()
    y = models.IntegerField()

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'stations'
