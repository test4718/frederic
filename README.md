# Frederic

- The postman collection is in `Frederic.postman_collection.json` file.

## Download Project

1. Clone project with command:

    ```git clone https://gitlab.com/test4718/frederic.git```


## Configurations



1. go to the project folder:

    ```cd frederic```

2. Create environment file `cp .env.example .env`

3. Modify the `.env` file with database configuration:

    ```
    DB_HOST=
    DB_PORT=
    DB_NAME=
    DB_USER=
    DB_PASS=
    ```

4. Modify the `.env` file with Frederic API configuration:

    - SPEED_DRIVER: frederic vehicle speed.
    - RADIUS_LIST: number list of radius.
    - LOCATIONS: endpoint with frederic locations.

    ```
    SPEED_DRIVER= 
    RADIUS_LIST=
    LOCATIONS=
    ```

## Docker install



1. Install [Docker](https://docs.docker.com/engine/install)

2. Install [Docker Compose](https://docs.docker.com/compose/install/)

3. Start docker container `docker-compose up -d`

4. Create database `docker-compose exec fre_db psql -U postgres -c 'create database frederic'`

5. Configuration steps.

6. Run migrations `docker-compose exec fre_api python manage.py migrate`

## Start local project



1.  Create virtual environment `python3 -m venv env` and activate virtual environment:

    - Linux: `source env/bin/activate`
    - Windows: `source env/Scripts/activate`

2.  Run command `pip3 install -r requirements.txt`

3.  Configuration steps.

4.  Run migrations `python3 manage.py migrate`



