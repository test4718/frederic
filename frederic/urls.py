from django.contrib import admin
from django.urls import path

from drivers.views.driver import DriverView
from orders.views.order import OrderView

urlpatterns = [
    path('orders', OrderView.as_view(), name='orders'),
    path('drivers', DriverView.as_view(), name='drivers'),
]
