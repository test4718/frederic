from django.db import transaction
from frederic.env import env

from rest_framework import serializers
from orders.models.order import Order
from frederic.find.routes import Route


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = (
            "id",
            "driver",
            "date_time",
            "x",
            "y",
            "arrival_time_min"
        )
        depth = 1


class CreateSerializer(serializers.Serializer):
    date_time = serializers.DateTimeField(required=True)
    x = serializers.IntegerField(required=True)
    y = serializers.IntegerField(required=True)

    def validate(self, attrs):
        attrs["route"] = Route(x=attrs["x"], y=attrs["y"], radius=env("RADIUS_LIST", list))

        if not "driver" in attrs["route"].best:
            raise serializers.ValidationError(detail="Drivers not found")
        return attrs

    @transaction.atomic
    def create(self, data):
        order = Order()
        order.date_time = data["date_time"]
        order.x = data["x"]
        order.y = data["y"]
        order.driver_id = data["route"].best["driver"]["id"]
        order.arrival_time_min = data["route"].best["arrival_time"]
        order.save()
        return order
