import django_filters


class DriverFilter(django_filters.FilterSet):

    driver = django_filters.NumberFilter(field_name='driver_id')
    date_time = django_filters.DateTimeFilter(field_name='date_time')
    state = django_filters.NumberFilter(method='filter_state')

    def filter_state(self, queryset, name, value):
        return queryset.filter(state__id=value)
