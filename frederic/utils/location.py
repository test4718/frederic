class Location:

    def __init__(self, x: int, y: int, radius_list: list):
        self.x = x
        self.y = y
        self.radius_list = radius_list

    def search(self, _in, i=0):

        for radius in self.radius_list:
            self.top = {"x": int(self.x)-int(radius), "y": int(self.y)-int(radius)}
            self.bottom = {"x": int(self.x)+int(radius), "y": int(self.y)+int(radius)}
            result = self._filter_radio(_in)
            if result:
                return result

        return []

    def _filter_radio(self, _in):
        return list(filter(self._filters, _in))

    def _filters(self, location):
        condition = (
            (
                location["x"] >= self.top["x"] and
                location["y"] >= self.top["y"]
            ) and
            (
                location["x"] <= self.bottom["x"] and
                location["y"] <= self.bottom["y"]
            )
        )
        if condition:
            location["distance"] = self._set_distance(location)
        return condition

    def _set_distance(self, location):
        y = abs(self.y - location["y"])
        x = abs(self.x - location["x"])
        return x+y
